$(function() {

    $(".owl-carousel").owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        navText: ['next','prev'],
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        smartSpeed: 800
    });

});