var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    less        = require('gulp-less'),
    base64      = require('gulp-base64'),
    autoprefix  = require('gulp-autoprefixer'),
    minifyCSS   = require('gulp-minify-css'),
    jsmin       = require('gulp-jsmin'),
    concat      = require('gulp-concat'),
    notify      = require("gulp-notify");

//var data_uri = require('gulp-data-uri');


var lessDir = 'assets/less';
var targetLessDir = 'public/css';

var jsDir = 'assets/js';
var targetJsDir = 'public/js';


gulp.task('css', function() {
    return gulp.src(lessDir + '/main.less')
        .pipe(less())
        .pipe(autoprefix('last 10 version'))
        .pipe(base64({
            debug: true,
            extensions: ['jpg', 'png', 'gif'],
        }))
        .pipe(minifyCSS({ keepSpecialComments: 0, removeEmpty: true }))
        .pipe(notify("Created <%= file.relative %>"))
        .pipe(gulp.dest(targetLessDir));
});

gulp.task('js', function() {
    return gulp.src(jsDir + '/**/*.js')
        .pipe(concat("app.js"))
        //.pipe(jsmin())
        .pipe(gulp.dest(targetJsDir));
});

gulp.task('watch', function() {
    gulp.watch(lessDir + '/**/*', ['css']);
    gulp.watch(jsDir + '/**/*.js', ['js']);
});

gulp.task('default', ['css', 'js', 'watch']);
