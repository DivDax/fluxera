<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Fluxera</title>

	<link rel="stylesheet" href="css/main.css">
	<script src="js/app.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<header>
	<div class="top">
		<div class="container">
			<div class="pull-right social">
				<ul class="list-inline">
					<li>
						<a href="#" class="text-lighter"><i class="fa fa-facebook-square fa-2x"></i></a>
					</li>
					<li>
						<a href="#" class="text-lighter"><i class="fa fa-xing-square fa-2x"></i></a>
					</li>
					<li>
						<a href="#" class="text-lighter"><i class="fa fa-youtube-square fa-2x"></i></a>
					</li>
					<li>
						<a href="#" class="text-lighter"><i class="fa fa-instagram fa-2x"></i></a>
					</li>
				</ul>
			</div>
			<a href="#" class="logo">Fluxera</a>
		</div>
		<div class="green-bg"></div>
	</div>

	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
<!--				<a class="navbar-brand" href="#">Fluxera</a>-->
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#">About</a></li>
<!--					<li><a href="#">Services</a></li>-->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Development</a></li>
							<li><a href="#">IT-Consulting</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Complete overview</a></li>
						</ul>
					</li>
					<li><a href="#">Products</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
</header>
