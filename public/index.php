<?php include '_header.php'; ?>

<div class="owl-carousel">
	<div class="item" style="background: url(//unsplash.it/2000/500?image=537) no-repeat center center;">
		<div class="bg">
			<div class="container">
				<h1>Webdevelopment</h1>
				<p class="lead">
					Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br>
					sed diam nonumy eirmod tempor invidunt ut labore et dolore<br>
					magna aliquyam erat, sed diam voluptua.
				</p>
				<a href="#" class="btn btn-primary btn-lg">Learn more</a>
			</div>
		</div>
	</div>
	<div class="item" style="background: url(//unsplash.it/2000/500?image=524) no-repeat center center;">

	</div>
</div>

<div id="main">
	<div class="container">

		<div class="row">
			<div class="col-sm-6 col-md-3">
				<h3><i class="fa fa-fw fa-code"></i> Development</h3>
				<p>
					Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
					sed diam nonumy eirmod tempor invidunt ut labore et dolore
					magna aliquyam erat, sed diam voluptua. At vero eos et accusam
					et justo duo dolores et ea rebum. Stet clita kasd gubergren.
				</p>
			</div>
			<div class="col-sm-6 col-md-3">
				<h3><i class="fa fa-fw fa-laptop"></i> IT-Consulting</h3>
				<p>
					Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
					sed diam nonumy eirmod tempor invidunt ut labore et dolore
					magna aliquyam erat, sed diam voluptua. At vero eos et accusam
					et justo duo dolores et ea rebum. Stet clita kasd gubergren.
				</p>
			</div>
			<div class="col-sm-6 col-md-3">
				<h3><i class="fa fa-fw fa-bar-chart"></i> Marketing</h3>
				<p>
					Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
					sed diam nonumy eirmod tempor invidunt ut labore et dolore
					magna aliquyam erat, sed diam voluptua. At vero eos et accusam
					et justo duo dolores et ea rebum. Stet clita kasd gubergren.
				</p>
			</div>
			<div class="col-sm-6 col-md-3">
				<h3><i class="fa fa-fw fa-support"></i> Support</h3>
				<p>
					Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
					sed diam nonumy eirmod tempor invidunt ut labore et dolore
					magna aliquyam erat, sed diam voluptua. At vero eos et accusam
					et justo duo dolores et ea rebum. Stet clita kasd gubergren.
				</p>
			</div>
		</div>

		<hr>

		<div class="row">
			<div class="col-sm-7">
				<h2>Development</h2>
				<p>
					Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
				</p>
			</div>
			<div class="col-sm-4 col-sm-offset-1">
				<img src="//unsplash.it/800/400?image=6" alt="" class="img-responsive">
			</div>
		</div>

		<hr>

		<h3>Contact</h3>

		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" id="name" class="form-control" value="">
		</div>
		<div class="form-group">
			<label for="mail">Email</label>
			<input type="text" id="mail" class="form-control" value="">
		</div>
		<div class="form-group">
			<label for="body">Message</label>
			<textarea class="form-control" rows="8" id="body"></textarea>
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" value="Send Message">
		</div>
	</div>
</div>

<?php include '_footer.php'; ?>