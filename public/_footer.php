<footer>
	<div class="container">
		<h5>Sitemap</h5>
		<div class="row">
			<div class="col-xs-6 col-sm-2">
				<ul class="list-unstyled">
					<li><a href="#">About</a></li>
					<li><a href="#">Services</a></li>
					<li><a href="#">Products</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-2">
				<ul class="list-unstyled">
					<li><a href="#">Team</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Newsletter</a></li>
					<li><a href="#">FAQ</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-2">
				<ul class="list-unstyled">
					<li><a href="#">Disclaimer</a></li>
					<li><a href="#">Imprint</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

</body>
</html>